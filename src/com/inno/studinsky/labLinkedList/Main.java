package com.inno.studinsky.labLinkedList;

import java.util.Iterator;

public class Main {

    public static void main(String[] args) {
        LabLinkedList<String> list = new LabLinkedList<>();
        list.add("Petr");
        list.add("Ivan");
        list.add("Nikolay");
        list.add(0, "Mikhail");
        list.add(2, "Alexandr");
        list.add("Oleg");
        list.add("Denis");

        System.out.println("вывод списка через объект итератор");
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            System.out.println(next);
        }

        list.remove("Ivan");
        list.remove(3);
        System.out.println("вывод списка через for");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        System.out.println("вывод списка через for each");
        for (String elem : list) {
            System.out.println(elem.toString());
        }
    }
}
