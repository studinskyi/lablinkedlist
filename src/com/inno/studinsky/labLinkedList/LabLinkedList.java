package com.inno.studinsky.labLinkedList;

import java.util.Iterator;

public class LabLinkedList<T> implements Iterable<T> {

    private Node<T> head; // первый элемент
    private Node<T> tail;  // последний элемент
    private int size = 0;  // размерность списка (количество узлов в связном списке)

    // класс узла Node
    private static class Node<T> {
        T item;  // элемент, хранящийся в узле
        Node<T> next;  // ссылка на следующий элемент
        Node<T> prev; // ссылка на предыдущий элемент

        // конструктор узла
        Node(Node<T> prev, T element, Node<T> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    // конструктор класса списка
    public LabLinkedList() {
    }

    // добавления элемента в конце списка
    public boolean add(T elem) {
        final Node<T> l = tail;
        final Node<T> newNode = new Node<>(l, elem, null);
        tail = newNode;
        if (l == null)
            // при пустом списке, добавляемый элемент должен являтся ещё и головным
            head = newNode;
        else
            // указать в хвостовом элементе что следующий элемент, это он же
            l.next = newNode;
        size++;
        return true;
    }

    // добавление элемента в середину списка по указанной в индексе позиции
    public void add(int index, T element) {
        // проверка попадания в размер массива
        if (!(index >= 0 && index < size))
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);

        if (index == size)
            // для частного случая вставки по индексу, но в конец массива
            add(element);
        else
            linkBefore(element, node(index));
    }

    // вспомогательный метод добавления в середину списка перед указанным элементом nodeNext
    void linkBefore(T elem, Node<T> nodeNext) {
        final Node<T> nodePrev = nodeNext.prev;
        final Node<T> newNode = new Node<>(nodePrev, elem, nodeNext);
        nodeNext.prev = newNode;
        if (nodePrev == null)
            head = newNode;
        else
            nodePrev.next = newNode;
        size++;
    }

    // вспомогательная функция получения узла по его индексу
    Node<T> node(int index) {
        if (index <= size) {
            Node<T> x = head;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            Node<T> x = tail;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }

    // удаление узла по индексу позиции узла в списке
    public T remove(int index) {
        // проверка попадания в размер массива
        if (!(index >= 0 && index < size))
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);

        return unlinkNode(node(index));
    }

    // удаление узла с элементом, равным указанному
    public boolean remove(Object objElement) {
        if (objElement == null) {
            for (Node<T> curNode = head; curNode != null; curNode = curNode.next) {
                if (curNode.item == null) {
                    unlinkNode(curNode);
                    return true;
                }
            }
        } else {
            for (Node<T> curNode = head; curNode != null; curNode = curNode.next) {
                if (objElement.equals(curNode.item)) {
                    unlinkNode(curNode);
                    return true;
                }
            }
        }
        return false;
    }

    // разлинкование (отвязка от ссылок) узла от связанных с ним предыдущего и следующего узла
    T unlinkNode(Node<T> x) {
        final T element = x.item;
        final Node<T> next = x.next;
        final Node<T> prev = x.prev;

        if (prev == null) {
            head = next;
        } else {
            prev.next = next;
            x.prev = null;
        }

        if (next == null) {
            tail = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }

        x.item = null;
        size--;
        return element;
    }

    // определяет содержится ли в списке передаваемый элемент
    public boolean contains(Object objElement) {
        return indexOf(objElement) != -1;
    }

    // возвращение индекса первого узла, в котором найден нужный элемент объекта (возврат -1 если элемент не был найден)
    public int indexOf(Object objElement) {
        int index = 0;
        if (objElement == null) {
            for (Node<T> curNode = head; curNode != null; curNode = curNode.next) {
                if (curNode.item == null)
                    return index;
                index++;
            }
        } else {
            for (Node<T> curNode = head; curNode != null; curNode = curNode.next) {
                if (objElement.equals(curNode.item))
                    return index;
                index++;
            }
        }
        return -1;
    }

    // получение элемента списка по индексу требуемого узла
    public T get(int index) {
        // проверка попадания в размер массива
        if (!(index >= 0 && index < size))
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        return node(index).item;
    }

    // возврат размерности списка (количества элементов списка)
    public int size() {
        return size;
    }

    @Override
    public Iterator<T> iterator() {
        Iterator<T> iterator = new Iterator<T>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size;
            }

            @Override
            public T next() {
                return get(currentIndex++);
            }

            @Override
            public void remove() {

            }
        };
        return iterator;
    }

}
